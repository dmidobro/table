import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import moment from 'moment'

const initialComplins = [{
    id: 'c1',
    problem: {id: 'p1', name: 'Problem 1', 'photoId1': 'nstiaenristn/31312312'},
    user: {id: 'u1', userName: 'John D'},
    cause: 'notRelevant',
    createdDate: '2020-01-10 13:37:26',
    active: false
},

    {
        id: 'c2',
        problem: {id: 'p2', name: 'Problem 2', 'photoId1': 'nstiaenristn/31312312'},
        user: {id: 'u2', userName: 'Mary C'},
        cause: 'notRelevant',
        createdDate: '2019-01-18 13:37:26',
        active: false
    },

    {
        id: 'c3',
        problem: {id: 'p3', name: 'Problem 3', 'photoId1': 'nstiaenristn/31312312'},
        user: {id: 'u3', userName: 'Tim K'},
        cause: 'notRelevant',
        createdDate: '2020-04-10 13:37:26',
        active: false
    },

    {
        id: 'c4',
        problem: {id: 'p1', name: 'Problem 1', 'photoId1': 'nstiaenristn/31312312'},
        user: {id: 'u2', userName: 'Mary C'},
        cause: 'notRelevant',
        createdDate: '2020-01-10 13:37:26',
        active: false
    },
    {
        id: 'c5',
        problem: {id: 'p2', name: 'Problem 2', 'photoId1': 'nstiaenristn/31312312'},
        user: {id: 'u3', userName: 'Tim K'},
        cause: 'nudity',
        createdDate: '2020-01-16 13:37:26',
        active: false
    }];


const useStyles = makeStyles({
    root: {
        minWidth: 275,
        maxWidth: 1024,
        margin: 200
    },

    txtAlign: {
        textAlign: "center"
    },
    tableCell: {
        "$hover:hover &": {
            color: "red"
        }
    },
    hover: {},
    active: {color: "red"},

});

function OutlinedCard() {
    const classes = useStyles();
    const [complains, setComplaints] = useState(initialComplins)

    const changeColor = (id) => {

        setComplaints(complains.map(complaine => (id === complaine.id ? {
            ...complaine,
            active: !complaine.active
        } : complaine)))
    };

    return (
        <Card className={classes.root} variant="outlined">
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="a dense table">
                    <TableHead >
                        <TableRow className={classes.txtAlign} >
                            <TableCell align="center">Problems</TableCell>
                            <TableCell align="center">Cause</TableCell>
                            <TableCell align="center">User</TableCell>
                            <TableCell align="center">CreateData</TableCell>
                            <TableCell align="center">Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {complains.map((complains) => (

                            <TableRow className={classes.tableRow} key={complains.id}>
                                <TableCell className={`${classes.tableCell} ${complains.active?classes.active:''}`} component="th" scope="row" align="left">
                                    {complains.problem.name}
                                </TableCell>
                                <TableCell className={`${classes.tableCell} ${complains.active?classes.active:''}`} align="center">{getCauseValue(complains.cause)}</TableCell>
                                <TableCell className={`${classes.tableCell} ${complains.active?classes.active:''}`} align="center">{complains.user.userName}</TableCell>
                                <TableCell className={`${classes.tableCell} ${complains.active?classes.active:''}`} align="center">{moment(complains.createdDate).calendar()}</TableCell>
                                <TableCell className={`${classes.tableCell} ${complains.active?classes.active:''}`} align="center"><Button size="small" color='secondary' variant='outlined' onClick={()=>changeColor(complains.id)}>Deactivate</Button></TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Card>

    );
}

const getCauseValue = (cause) => {
    if(cause === 'notRelevant' ){
        return "Not Relevant"
    }else if(cause === 'nudity'){
        return 'Nudity'
    }
}

export default OutlinedCard;